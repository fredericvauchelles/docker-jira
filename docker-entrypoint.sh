#!/bin/bash
set -e

mkdir -p "$CONF_HOME";
chmod 700 "$CONF_HOME";
chown -R jira: "$CONF_HOME";

exec gosu jira "${CONF_INSTALL}/bin/start-jira.sh" "-fg";

exec "$@"
